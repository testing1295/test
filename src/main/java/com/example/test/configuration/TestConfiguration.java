package com.example.test.configuration;

import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Collections.emptySet;
import static java.util.UUID.randomUUID;
import static org.slf4j.LoggerFactory.getLogger;

@Configuration
public class TestConfiguration {
    private static final Logger LOG = getLogger(TestConfiguration.class);

    private final UserRepository userRepository;

    @Autowired
    public TestConfiguration(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Bean
    InitializingBean createUsersInDatabase() {
        return () -> {
            LOG.info("Initializing db with 2 users as no endpoint is asked to create them");
            userRepository.save(new User(randomUUID(), "John", "Doe", emptySet()));
            userRepository.save(new User(randomUUID(), "Foo", "Bar", emptySet()));
        };
    }
}


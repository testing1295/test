package com.example.test.repository;

import com.example.test.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.jpa.repository.EntityGraph.EntityGraphType.LOAD;

public interface UserRepository extends CrudRepository<User, UUID>  {

    @EntityGraph(value = "user-with-account-transaction", type = LOAD)
    Optional<User> findWithAccountTransationByCustomerId(UUID customerId);

}

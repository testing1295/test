package com.example.test.controller;

import com.example.test.dto.AccountDTO;
import com.example.test.dto.TransactionDTO;
import com.example.test.dto.UserDTO;
import com.example.test.model.Account;
import com.example.test.model.Transaction;
import com.example.test.model.User;
import com.example.test.service.AccountService;
import com.example.test.service.UserService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static java.math.BigDecimal.ZERO;
import static java.util.Collections.emptySet;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.beans.BeanUtils.copyProperties;

@RestController
@RequestMapping("/api")
public class UserController {

    private static final Logger LOG = getLogger(UserController.class);

    private final AccountService accountService;

    private final UserService userService;

    @Autowired
    public UserController(AccountService accountService, UserService userService) {
        this.accountService = accountService;
        this.userService = userService;
    }

    @PostMapping(path = "/accounts")
    public AccountDTO createAccount(@RequestBody UserDTO userDTO) {
        Account accountForUser = accountService.createAccountForUser(userDTO);
        LOG.info("Created an account for customer : {}", userDTO.getCustomerId());
        return convertToDto(accountForUser);
    }

    private static AccountDTO convertToDto(Account accountForUser) {
        AccountDTO accountDTO = new AccountDTO();
        copyProperties(accountForUser, accountDTO);
        Set<Transaction> transactionSet = ofNullable(accountForUser.getTransactionSet()).orElse(emptySet());
        accountDTO.setTransactionList(transactionSet.stream().map(transaction -> {
                    TransactionDTO transactionDTO = new TransactionDTO();
                    copyProperties(transaction, transactionDTO);
                    return transactionDTO;
                }).collect(toList()));
        accountDTO.setBalance(ofNullable(accountForUser.getTransactionSet())
                .map(transactions ->
                    transactions.stream().map(Transaction::getAmount).reduce(ZERO, BigDecimal::add)
                ).orElse(ZERO));
        return accountDTO;
    }

    @GetMapping(path = "/users")
    public List<UserDTO> getAllUsers() {
        Iterable<User> listUsers = userService.getAllUsers();
        return stream(listUsers.spliterator(), false).map(UserController::convertToDto).collect(toList());
    }

    private static UserDTO convertToDto(User user) {
        UserDTO returned = new UserDTO();
        copyProperties(user, returned);
        return returned;
    }

    @GetMapping(path = "/users/{customerId}")
    public UserDTO getUser(@PathVariable UUID customerId) {
        User user = userService.getUser(customerId);
        UserDTO returned = convertToDto(user);
        Set<Account> accountSet = ofNullable(user.getAccountSet()).orElse(emptySet());
        returned.setAccountList(accountSet.stream().map(UserController::convertToDto).collect(toList()));
        return returned;
    }

}

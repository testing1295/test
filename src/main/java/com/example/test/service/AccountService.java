package com.example.test.service;

import com.example.test.dto.UserDTO;
import com.example.test.model.Account;

public interface AccountService {

    Account createAccountForUser(UserDTO userDTO);
}

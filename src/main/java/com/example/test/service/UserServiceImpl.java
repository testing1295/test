package com.example.test.service;

import com.example.test.exception.TestException;
import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUser(UUID customerId) {
        return userRepository.findWithAccountTransationByCustomerId(customerId)
                .orElseThrow(() -> new TestException("no user found for customer id : " + customerId));
    }

    @Override
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }
}

package com.example.test.service;

import com.example.test.model.User;

import java.util.UUID;

public interface UserService {
    User getUser(UUID customerId);

    Iterable<User> getAllUsers();
}

package com.example.test.service;

import com.example.test.dto.UserDTO;
import com.example.test.exception.TestException;
import com.example.test.model.Account;
import com.example.test.model.Transaction;
import com.example.test.model.User;
import com.example.test.repository.AccountRepository;
import com.example.test.repository.UserRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

import static java.math.BigDecimal.ZERO;
import static java.util.Set.of;
import static java.util.UUID.randomUUID;
import static org.slf4j.LoggerFactory.getLogger;

@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger LOG = getLogger(AccountServiceImpl.class);

    private final AccountRepository accountRepository;

    private final UserRepository userRepository;

    private final TransactionService transactionService;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, UserRepository userRepository, TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.transactionService = transactionService;
    }

    @Override
    public Account createAccountForUser(UserDTO userDTO) {
        UUID customerId = userDTO.getCustomerId();
        return userRepository.findById(customerId)
                .map(user -> createAccountImpl(userDTO, user))
                .orElseThrow(() -> new TestException("no user found for customer id : " + customerId));
    }

    private Account createAccountImpl(UserDTO userDTO, User user) {
        Account account = new Account();
        account.setId(randomUUID());
        account.setUser(user);
        Account savedAccount = accountRepository.save(account);
        BigDecimal initialCredit = userDTO.getInitialCredit();
        if (!initialCredit.equals(ZERO)) {
            LOG.info("Initial credit is : {}, different than zero, creating a transaction", initialCredit);
            Transaction savedTransaction = transactionService.createTransaction(savedAccount, initialCredit);
            savedAccount.setTransactionSet(of(savedTransaction));
        }
        return savedAccount;
    }

}

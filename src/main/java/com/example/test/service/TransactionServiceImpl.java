package com.example.test.service;

import com.example.test.model.Account;
import com.example.test.model.Transaction;
import com.example.test.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static java.util.UUID.randomUUID;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction createTransaction(Account savedAccount, BigDecimal initialCredit) {
        Transaction transaction = new Transaction();
        transaction.setId(randomUUID());
        transaction.setAccount(savedAccount);
        transaction.setAmount(initialCredit);
        return transactionRepository.save(transaction);
    }

}

package com.example.test.service;

import com.example.test.model.Account;
import com.example.test.model.Transaction;

import java.math.BigDecimal;

public interface TransactionService {

    Transaction createTransaction(Account savedAccount, BigDecimal initialCredit);

}

package com.example.test.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "USERS")
@NamedEntityGraph(name = "user-with-account-transaction",
        attributeNodes = {
                @NamedAttributeNode(value = "accountSet", subgraph = "accountSet.transactionSet")
        },
        subgraphs = {
                @NamedSubgraph(name = "accountSet.transactionSet", attributeNodes = @NamedAttributeNode(value = "transactionSet")),
        })
public class User {

    @Id
    private UUID customerId;

    private String name;

    private String surname;

    @OneToMany(mappedBy = "user")
    private Set<Account> accountSet;
}

package com.example.test.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
import java.util.UUID;

import static jakarta.persistence.FetchType.LAZY;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "ACCOUNTS")
public class Account {

    @Id
    private UUID id;

    @OneToMany(mappedBy = "account")
    private Set<Transaction> transactionSet;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "customer_id")
    private User user;

}

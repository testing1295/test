package com.example.test.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

import static jakarta.persistence.FetchType.LAZY;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "TRANSACTIONS")
public class Transaction {

    @Id
    private UUID id;

    private BigDecimal amount;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "account_uuid")
    private Account account;

}

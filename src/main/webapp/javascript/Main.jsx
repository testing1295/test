import React, {useEffect, useState} from "react";
import ReactDOM from 'react-dom';
import '../css/Main.css';

function Main() {
  const [userList, setUserList] = useState([]);
  const [customerId, setCustomerId] = useState("");
  const [initialCredit, setInitialCredit] = useState(0);
  const [userChanged, setUserChanged] = useState();

  useEffect(() => {
    fetch("/api/users").then((res) => res.json())
      .then((resJson) => setUserList(resJson));
  }, []);

  const changeCustomerId = (e) => {
    setCustomerId(e.target.value);
  };

  const changeInitialCredit = (e) => {
    setInitialCredit(e.target.value);
  };

  const createNewAccount = () => {
    fetch("/api/accounts", {
      method: "POST",
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
      },
      body: JSON.stringify({
        customerId,
        initialCredit,
      }),
    }).then(() => fetch("/api/users/" + customerId))
      .then((res) => res.json())
      .then((resJson) => setUserChanged(resJson));
  };

  return (
    <div id="main">
      <h1>Back-end coding assignment</h1>
      <ul>
        <div>Actual list of users in the database (for tests) :</div>
        {userList.map((user) => (<li key={user.name}><span>{user.name} with customerId : </span><span>{user.customerId}</span></li>))}
      </ul>
      <label htmlFor="customerId">customerId :</label>
      <input name="customerId" type="text" onChange={changeCustomerId} value={customerId}/>
      <label htmlFor="initalCredit">initalCredit :</label>
      <input name="initialCredit" type="text" onChange={changeInitialCredit} value={initialCredit}/>
      <input type="button" name="createNewAccount" onClick={createNewAccount} value="Create a new account"/>
      {userChanged && (
        <div>The following user has now a new account, here is the summary of his situation :
          <div>{userChanged.name} {userChanged.surname}</div>
          <div>{userChanged.accountList.length} {userChanged.accountList.length > 0 ? "accounts" : "account"} :
            <ul>
              {userChanged.accountList.map((account, index) => (
                <li>
                  <div><span>account {index + 1} with {account.transactionList ? account.transactionList.length : 0} {account.transactionList
                    && account.transactionList.length > 0 ? "transactions" : "transaction"} has for balance : {account.balance}</span>
                    <ul>{account.transactionList?.map((transaction, index) => (
                      <li>transaction {index + 1} with amount : {transaction.amount}</li>
                    ))}
                    </ul>
                  </div>
                </li>
              ))}
            </ul>
          </div>

        </div>
      )
      }
    </div>
  );
}

ReactDOM.render(
  <Main/>,
  document.getElementById('react-mountpoint')
);

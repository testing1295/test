package com.example.test;

import com.example.test.controller.UserController;
import com.example.test.dto.AccountDTO;
import com.example.test.dto.UserDTO;
import com.example.test.model.Account;
import com.example.test.model.Transaction;
import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import com.example.test.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static java.math.BigDecimal.ZERO;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TestApplicationTests {

	public static final BigDecimal INITIAL_CREDIT = new BigDecimal("-100.45");
	@Autowired
	UserRepository userRepository;

	@Autowired
	AccountService accountService;

	@Autowired
	UserController userController;

	@Test
	void contextLoads() {
	}

	@Test
	public void createAccountForUser() {
		Iterable<User> all = userRepository.findAll();
		User next = all.iterator().next();
		UserDTO userDTO = new UserDTO();
		UUID customerId = next.getCustomerId();
		userDTO.setCustomerId(customerId);
		userDTO.setInitialCredit(ZERO);
		Account accountForUser = accountService.createAccountForUser(userDTO);
		assertNull(accountForUser.getTransactionSet(), "a transaction has been created");
	}

	@Test
	public void create2ndAccountForUserWithTransaction() {
		Iterable<User> all = userRepository.findAll();
		User next = all.iterator().next();
		UserDTO userDTO = new UserDTO();
		UUID customerId = next.getCustomerId();
		userDTO.setCustomerId(customerId);
		userDTO.setInitialCredit(INITIAL_CREDIT);
		Account accountForUser = accountService.createAccountForUser(userDTO);
		assertEquals(1, accountForUser.getTransactionSet().size(), "the transaction has not been created once");
		assertEquals(INITIAL_CREDIT, accountForUser.getTransactionSet().iterator().next().getAmount(), "the transaction does not have the correct amount");
		Optional<User> optUser = userRepository.findWithAccountTransationByCustomerId(customerId);
		User user = optUser.get();
		Set<Account> accountSet = user.getAccountSet();
		assertEquals(2, accountSet.size(), "the accounts have not been created twice");
		List<Account> accountsWithTransactions = accountSet.stream().filter(account -> account.getTransactionSet().size() > 0).toList();
		assertEquals(1, accountsWithTransactions.size(), "there should be only one account with a transaction");
		Set<Transaction> transactionSet = accountsWithTransactions.get(0).getTransactionSet();
		assertEquals(1, transactionSet.size(), "the transaction has not been created once");
		assertEquals(0, INITIAL_CREDIT.compareTo(transactionSet.iterator().next().getAmount()));
	}

	@Test
	public void getUser() {
		Iterable<User> all = userRepository.findAll();
		User next = all.iterator().next();
		UserDTO user = userController.getUser(next.getCustomerId());
		assertNotNull(user, "user has been returned");
		List<AccountDTO> accountList = user.getAccountList();
		assertEquals(2, accountList.size(), "user does not have 2 accounts");
		accountList.forEach((accountDTO -> {
			if (accountDTO.getTransactionList().isEmpty()) {
				assertEquals(ZERO, accountDTO.getBalance());
			} else {
				assertEquals(INITIAL_CREDIT, accountDTO.getBalance());
			}
		}));
	}


}

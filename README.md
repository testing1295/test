# Back-end coding assignment

## Installation

The project has been developed with java-17-amazon-corretto, it should be the preferred version.

In order to launch spring boot :

`./gradlew bootRun`

then access http://localhost:8080/


For installation with docker :

`docker build . -t test`

and

`docker run -p 8080:8080 test`

## Test

On the web page, is is possible to test the application by entering a customerId (one of the UUIDs displayed) and an initialCredit.

It will then display the state of the user.

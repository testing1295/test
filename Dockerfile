FROM gradle:jdk17 AS build
ENV APP_HOME=/home/gradle/src/
WORKDIR $APP_HOME
COPY --chown=gradle:gradle . $APP_HOME
RUN gradle build --no-daemon

FROM amazoncorretto:17-alpine3.16

EXPOSE 8080

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/test-1.0.jar /app/spring-boot-application.jar

ENTRYPOINT ["java", "-jar", "/app/spring-boot-application.jar"]